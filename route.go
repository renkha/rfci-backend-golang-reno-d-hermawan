package main

import (
    // "log"
    "net/http"
	"encoding/json"
    // "github.com/gorilla/mux"
)

func no1(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set("Content-Type", "application/json")
	array := [...]int{4, 9, 7, 5, 8, 9, 3}
	var swap int
	for i := 0; i < len(array) - 1; i++ {
		if array[i] >= array[i+1] {
			if i == len(array) {
				break;
			} else {
				array[i] = array[i+1]
				swap = swap + 1
			}
			result, err := json.Marshal(swap)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(`{"error": "error marshalling data"}`))
				return
			}
			w.WriteHeader(http.StatusOK)
			w.Write(result)
			return
		}
	}
}

func no2(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusNotFound)
    w.Write([]byte(`{"message": "not done yet"}`))
}

func no3(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{"message": "inside file fixing"}`))
}
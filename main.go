package main

import (
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

func main() {
    r := mux.NewRouter()
    api := r.PathPrefix("/api/v1").Subrouter()
    api.HandleFunc("/no1", no1).Methods(http.MethodGet)
    api.HandleFunc("/no2", no2).Methods(http.MethodPost)
    api.HandleFunc("/no3", no3).Methods(http.MethodGet)
    log.Fatal(http.ListenAndServe(":8080", r))
}